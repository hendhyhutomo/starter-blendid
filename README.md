#  PROJECT NAME

Website Development Project for PROJECTNAME
Started START DATE

THIS IS A STARTER PROJECT

Made using Yarn and Blendid

## Quick start on a fresh project

```sh
$ yarn init
$ yarn add blendid
$ yarn run blendid -- init
```
Or just setup

```sh
$ yarn install
```

## Run Project

```sh
yarn run blendid
```

More Info on [Blendid](https://github.com/vigetlabs/blendid)
