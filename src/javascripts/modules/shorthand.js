// QUERY SELECTOR SHORTHAND
const sQ = (_selector) => {
    return document.querySelector(_selector);
}

//ADD CLASS FUNCTION
const addC = (elem, classname) =>{
    if( sQ(elem) != null){
      const _elem = sQ(elem).classList;
      if(!_elem.contains(classname))_elem.add(classname);
    }
}

//TOGGLE CLASS ON OFF FUNCTION
const toggleC = (elem, classname) =>{
    if( sQ(elem) != null){
        let _elem = sQ(elem).classList;
        if(elem === "body"){
            _elem = document.body.classList;
        }
        if(!_elem.contains(classname)){
            _elem.add(classname);
        }
        else{
            _elem.remove(classname);
        }
    }
}

const toggleCE = (elem, classname) =>{
    if( elem != null){
        let _elem = elem.classList;
        if(elem === "body"){
            _elem = document.body.classList;
        }
        if(!_elem.contains(classname)){
            _elem.add(classname);
        }
        else{
            _elem.remove(classname);
        }
    }
}
//ADD CLASS FUNCTION based on Element
const addCE = (elem, classtarget) =>{
    if(elem != null && !elem.classList.contains(classtarget))elem.classList.add(classtarget);
}
//CHECK TOUCH DEVICE
const isTouchDevice = () => {
    return 'ontouchstart' in document.documentElement;
}

//DISABLE CLICKK ON DRAG.
// MUST ADD CSS POINTER EVENT NONE WITH CLASS DISABLE CLICK

const DisableClickOnDrag = (target, mobilecheck = false, mobilewidth = 650) => {
    let __target;

    if(!target){
        __target = sQ(target);
    }
    else{
        __target = target;
    }

    let disableTimer;
    let disableCheck = false;
    let disableAll = false;
    let __onmobile = false;


    __target.addEventListener('mousedown', (event) =>{
        //CHECK MOBILE
        if(window.innerWidth < mobilewidth ){
            __onmobile = true;
        }
        else{
            __onmobile = false;
        }
        if(mobilecheck && __onmobile){
            disableAll = true;
        }
        if(!disableAll && !disableCheck && __target.scrollWidth > __target.clientWidth){
            // console.log('disable', __target.scrollWidth, __target.clientWidth);
            if(disableTimer)clearTimeout(disableTimer);
            disableTimer = setTimeout(() => {
                disableCheck = true;
                if(!__target.classList.contains('disable-click')){
                    __target.classList.add('disable-click');
                }
            }, 250);
        }
    }, false);

    __target.addEventListener('mouseup', (event) =>{
        if(disableCheck){
            if(disableTimer)clearTimeout(disableTimer);
            disableTimer = setTimeout(() => {
                disableCheck = false;
                __target.classList.remove('disable-click');
            }, 50);
        }
    }, false);
}


var keys = {37: 1, 38: 1, 39: 1, 40: 1};

const preventDefault = (e) => {
  e = e || window.event;
  if (e.preventDefault)
      e.preventDefault();
  e.returnValue = false;
}

const preventDefaultForScrollKeys = (e) => {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

const disableScroll = (e) =>  {
  if (window.addEventListener) // older FF
      window.addEventListener('DOMMouseScroll', preventDefault, false);
  window.onwheel = preventDefault; // modern standard
  window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
  window.ontouchmove  = preventDefault; // mobile
  document.onkeydown  = preventDefaultForScrollKeys;
}

const enableScroll = (e) =>  {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}

const CheckImagesLoaded = (imagetag, endCallBack, loadonerror = true ) => {

    let imgs = document.querySelectorAll(imagetag),
    len = imgs.length,
    counter = 0;

    const incrementCounter = (target, loaded) => {
        counter++;
        if ( counter === len ) {
            finishLoading();
        }
    }
    const finishLoading = () => {
        endCallBack();
    }

    if(len === 0){
        finishLoading();
    }
    else{
        imgs.forEach(function(eachimg) {
            if(eachimg && !eachimg.complete){
                eachimg.onload = function() {
                    incrementCounter(eachimg, true);
                }
                if(loadonerror){
                    eachimg.onerror = function() {
                        incrementCounter(eachimg, false);
                    }
                }
                // eachimg.addEventListener( 'load', incrementCounter, false );
            }
            else{
                incrementCounter(eachimg, true);
            }
        });
    }
}

//TRIGGER PROGRESSIVE IMAGE LOAD
const CheckProgressiveImages = (imagetag, errorskip) => {
    const AllProgressiveImages = document.querySelectorAll(imagetag);

    const removeProgressiveLoad = (__image) => {
        addCE(__image.parentNode, 'loaded');
    }
    AllProgressiveImages.forEach((progressiveImage, index)=>{

        addCE(progressiveImage.parentNode, 'progressive_load_wrapper');

        if(progressiveImage && !progressiveImage.complete){
            progressiveImage.onload = () => {
                removeProgressiveLoad(progressiveImage);
            }
            if(errorskip){
                progressiveImage.onerror = () => {
                    removeProgressiveLoad(progressiveImage);
                }
            }
            if(progressiveImage.dataset.src){
                progressiveImage.src = progressiveImage.dataset.src;
            }
        }
        else{
            // IF IMAGE IS ALREADY LOADED
            removeProgressiveLoad(progressiveImage);
        }
    })
}


const getAllChildrenHeight = (target) => {
    const targetChildren = [].slice.call(target.children);
    let _height = 0;

    targetChildren.forEach((targetChild, index)=> {
        _height = _height + targetChild.offsetHeight;
    });

    return _height;
}


const TriggerDropdown = {
    init: (parent = document) => {
        if(parent.classList.contains('dropdown__mobileonly')){
            const dropdownTrigger = parent.querySelector('.dropdown__trigger');
            const dropdownWrapper = parent.querySelector('.dropdown__wrapper');

            dropdownTrigger.onclick = () =>{
                dropdownToggle();
            }

            const dropdownToggle = () => {

                if(parent.classList.contains('dropdown_open')){
                    TriggerDropdown.close(parent);
                }
                else{
                    TriggerDropdown.open(parent);
                }
            }
            document.addEventListener("scroll", () => {
                TriggerDropdown.heightadjust(parent);
            }, false);
        }
    },
    close: (parent) => {
        if(parent.classList.contains('dropdown__mobileonly')){
            const dropdownWrapper = parent.querySelector('.dropdown__wrapper');
            let _maxHeight = getAllChildrenHeight(dropdownWrapper);
            dropdownWrapper.style.height = _maxHeight + "px";
            TriggerDropdown.heightadjust(parent);
            $(document).off('click');
            parent.classList.remove('dropdown_open');
        }
    },
    open: (parent) => {
        if(parent.classList.contains('dropdown__mobileonly')){
            const dropdownWrapper = parent.querySelector('.dropdown__wrapper');
            let _maxHeight = getAllChildrenHeight(dropdownWrapper);
            dropdownWrapper.style.height = _maxHeight + "px";
            TriggerDropdown.heightadjust(parent);
            addCE(parent, 'dropdown_open');
            $(document).on('click', (event) => {
                if (!$(event.target).closest(parent).length) {
                    dropdownToggle();
                }
            });
        }
    },
    heightadjust: (parent) => {
        if(parent.classList.contains('dropdown__mobileonly')){
            const dropdownWrapper = parent.querySelector('.dropdown__wrapper');
            let _maxHeight = window.innerHeight - dropdownWrapper.offsetTop;
            dropdownWrapper.style.maxHeight = _maxHeight + "px";
            console.log('toggle max height');
        }
    }
}

export {
    sQ,
    addC,
    addCE,
    toggleC,
    toggleCE,
    isTouchDevice,
    DisableClickOnDrag,
    enableScroll,
    disableScroll,
    CheckImagesLoaded,
    CheckProgressiveImages,
    TriggerDropdown,
    getAllChildrenHeight
};
